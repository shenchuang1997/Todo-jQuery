/*
* @Author: sc
* @Date:   2018-12-28 20:22:26
* @Last Modified by:   sc
* @Last Modified time: 2018-12-28 23:49:26
*/

'use strict';
var Hogan = require('hogan.js');
var _mm = {
	//渲染html模版
	renderHtml : function(htmlTemplate , data){
		var template  = Hogan.compile(htmlTemplate),
			result    = template.render(data);
		return result;
	},
	// 初始化存储
	initStorage : function(){
		this.saveData('count-new', 1);
		this.saveData('count-done', 1);
	},
	// 清理缓存
	clearStorage : function(){
		localStorage.clear();
	},
	createJson : function(data){
		var count  = parseInt(this.getData('count-new'));
		var myDate = new Date(),
			result = {
				'num'   : count,
				'data'  : data,
				'ctime' : myDate.toLocaleString(),
				'dtime' : ''
			};
		//alert(JSON.stringify(result))
		return	result;
	},
	// H5 localStorage
	saveData : function(namespace,data){
		// 先判断是否支持
		if (window.localStorage)
		{
			localStorage.setItem(namespace, data);
		}else{
			alert('此浏览器不支持H5存储');
		}
	},
	getData : function(namespace){
		var data = localStorage.getItem(namespace);
		if(data){
			return data;
		}
		return null;
	},
	deleteData : function(namespace){
		localStorage.setItem(namespace, 'delete');
	}
	
};

module.exports = _mm;