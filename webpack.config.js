/*
* @Author: sc
* @Date:   2018-12-28 18:24:47
* @Last Modified by:   sc
* @Last Modified time: 2018-12-29 00:22:06
*/

'use strict';
var webpack           = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var htmlWebpackPlugin = require('html-webpack-plugin')
//环境变量的配置，dev/online
var WEBPACK_ENV       = process.env.WEBPACK_ENV || 'dev';
console.log(WEBPACK_ENV);
//获取html-webpack-plugin参数的方法
var getHtmlConfig = function(name, title){
	return {
      	template : './src/view/' + name + '.html',
      	filename : 'view/' + name + '.html',
      	title    : title,
      	inject 	 : true,
      	hash     : true,
      	chunks   : ['index',name]
	};
}
//webpack config
var config = {
	entry : {
		'index' 			: ['./src/page/index/index.js'],
	},
	output : {
		path: './dist',
		publicPath : '/dist',
		filename: 'js/[name].js'
	},
	externals : {
		'jquery' : 'window.jQuery'
	},

	module : {
		loaders: [
			{ test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") } ,			
			{ test: /\.(gif|png|jpg|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=100&name=/resource/[name].[ext]'},
			{ test: /\.string$/, loader: 'html-loader' } 	 
	    ]
	},
	resolve : {
		alias : {
			node_modules 	: __dirname + '/node_modules',
			util    	 	: __dirname + '/src/util',
			page    		: __dirname + '/src/page',
		}
	},
	plugins : [
		//把css单独打包到文件
		new ExtractTextPlugin("css/[name].css"),
		//html模版的处理
		new htmlWebpackPlugin(getHtmlConfig('index', '首页'))
	]
};

if('dev' === WEBPACK_ENV){
	config.entry.index.push('webpack-dev-server/client?http://localhost:8088/');
}
module.exports = config;