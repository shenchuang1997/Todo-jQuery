# Todo总结
## 一、技术选型
1. jQuery：选择器和事件处理
2. Bootstrap：响应式的样式
3. Hogan：Html模版渲染
4. webpack：模块化开发和实时刷新网页

## 二、学到的东西
1. node环境的配置，如何初始化项目。
2. webpack的安装，webpack如何处理js、css、图片以及html模版。
3. jQuery的基本用法。
4. Bootstrap的基本用法。
5. Hogan的基本用法。
6. H5的缓存机制，localStorage的基本用法：
   + 保存/修改：localStorage.saveItem(key, value);
   + 删除：localStorage.removeItem(key, value);
   + 清理缓存：localStorage.clear();

7. localStorage存储JSON时会把JSON保存为字符串，所以需要对字符串进行转换，下面是一些常用方法：
   + 使用jQuery：$.parseJSON(jsonstr);
   + 使用浏览器自带的转换方式（Firefox，chrome，opera，safari，ie）：JSON.parse(jsonstr);        
   + JSON.stringify(json);
   + Javascript支持的转换方式：eval('(' + jsonstr + ')'); 
   + JSON官方的转换方式：json.js，stringify()和parse()

8. 在文字中划线的css样式：text-decoration: line-through;
## 三、项目演示
![输入图片说明](https://images.gitee.com/uploads/images/2018/1229/004438_595cf202_1766072.gif "演示.gif")

**演示地址**：https://codepen.io/deep-moon/full/LMzmXG
